﻿using Kruma.Core.Data;
using Kruma.Core.Data.Entity;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Data
{

   
    public class RespuestaDAO
    {
       
        /// <summary>Listar Respuesta</summary>
        /// <param name="int_pIdRespuesta">IdRespuesta</param>
        /// <param name="int_pIdPersona">IdPersona</param>
        /// <param name="str_pMensaje">Mensaje</param>
        /// <param name="int_pTiempo">Tiempo</param>
        /// <param name="int_pCosto">Costo</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de Respuesta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<RespuestaBE> ListarRespuesta(
            int? int_pIdRespuesta,
            int? int_pIdPersona,
            string str_pMensaje,
            int? int_pTiempo,
            int? int_pCosto,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            Kruma.Core.Util.Common.List<RespuestaBE> obj_Lista = new Kruma.Core.Util.Common.List<RespuestaBE>();
            obj_Lista.PageNumber = int_pNumPagina;
            obj_Lista.Total = 0;

            DataOperation dop_Operacion = new DataOperation("ListarRespuesta");
            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuesta", int_pIdRespuesta.HasValue ? int_pIdRespuesta.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pIdPersona", int_pIdPersona.HasValue ? int_pIdPersona.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pMensaje", !string.IsNullOrEmpty(str_pMensaje) ? str_pMensaje : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTiempo", int_pTiempo.HasValue ? int_pTiempo.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pCosto", int_pCosto.HasValue ? int_pCosto.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", !string.IsNullOrEmpty(str_pEstado) ? str_pEstado : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pNumPagina", int_pNumPagina.HasValue ? int_pNumPagina.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTamPagina", int_pTamPagina.HasValue ? int_pTamPagina.Value : (object)DBNull.Value));

            DataTable dt_Resultado = DataManager.ExecuteDataSet(Conexion.CO_SistemaFluVen, dop_Operacion).Tables[0];

            List<RespuestaBE> lst_RespuestaOpcion = new List<RespuestaBE>();
            RespuestaBE obj_Respuesta = new RespuestaBE();
            foreach (DataRow obj_Row in dt_Resultado.Rows)
            {
                if (lst_RespuestaOpcion.Count == 0)
                    obj_Lista.Total = (int)obj_Row["Total_Filas"];
                obj_Respuesta = new RespuestaBE();
                obj_Respuesta.IdPersona = obj_Row["IdPersona"] is DBNull ? null : (int?)obj_Row["IdPersona"];
                obj_Respuesta.IdRespuesta = obj_Row["IdRespuesta"] is DBNull ? null : (int?)obj_Row["IdRespuesta"];
                obj_Respuesta.Mensaje = obj_Row["Mensaje"] is DBNull ? null : obj_Row["Mensaje"].ToString();
                obj_Respuesta.Tiempo = obj_Row["Tiempo"] is DBNull ? null : (Decimal?)obj_Row["Tiempo"];
                obj_Respuesta.Costo = obj_Row["Costo"] is DBNull ? null : (Decimal?)obj_Row["Costo"];
                obj_Respuesta.Estado = obj_Row["Estado"] is DBNull ? null : obj_Row["Estado"].ToString();
                obj_Respuesta.UsuarioCreacion = obj_Row["UsuarioCreacion"] is DBNull ? null : obj_Row["UsuarioCreacion"].ToString();
                obj_Respuesta.FechaCreacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaCreacion"] is DBNull ? null : (DateTime?)obj_Row["FechaCreacion"]);
                obj_Respuesta.UsuarioModificacion = obj_Row["UsuarioModificacion"] is DBNull ? null : obj_Row["UsuarioModificacion"].ToString();
                obj_Respuesta.FechaModificacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaModificacion"] is DBNull ? null : (DateTime?)obj_Row["FechaModificacion"]);

                lst_RespuestaOpcion.Add(obj_Respuesta);
            }

            obj_Lista.Result = lst_RespuestaOpcion;
            return obj_Lista;
        }

        /// <summary>Insertar Respuesta</summary>
        /// <param name="obj_pRespuesta">Respuesta</param>
        /// <returns>Id de Respuesta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static int Insertar(RespuestaBE obj_pRespuesta)
        {
            DataOperation dop_Operacion = new DataOperation("InsertarRespuesta");
            dop_Operacion.Parameters.Add(new Parameter("@pIdPersona", obj_pRespuesta.IdPersona));
            dop_Operacion.Parameters.Add(new Parameter("@pMensaje", obj_pRespuesta.Mensaje));
            dop_Operacion.Parameters.Add(new Parameter("@pTiempo", obj_pRespuesta.Tiempo));
            dop_Operacion.Parameters.Add(new Parameter("@pCosto", obj_pRespuesta.Costo));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioCreacion", obj_pRespuesta.UsuarioCreacion));

            Parameter obj_IdRespuesta = new Parameter("@pIdRespuesta", DbType.Int32);
            obj_IdRespuesta.Direction = ParameterDirection.Output;
            dop_Operacion.Parameters.Add(obj_IdRespuesta);

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
            int int_IdRespuesta = (int)obj_IdRespuesta.Value;
            return int_IdRespuesta;
        }

        /// <summary>Actualizar Respuesta</summary>
        /// <param name="obj_pRespuesta">Respuesta</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static void Modificar(RespuestaBE obj_pRespuesta)
        {
            DataOperation dop_Operacion = new DataOperation("ActualizarRespuesta");

            dop_Operacion.Parameters.Add(new Parameter("@pIdPersona", obj_pRespuesta.IdPersona));
            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuesta", obj_pRespuesta.IdRespuesta));
            dop_Operacion.Parameters.Add(new Parameter("@pMensaje", obj_pRespuesta.Mensaje));
            dop_Operacion.Parameters.Add(new Parameter("@pTiempo", obj_pRespuesta.Tiempo));
            dop_Operacion.Parameters.Add(new Parameter("@pCosto", obj_pRespuesta.Costo));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioModificacion", obj_pRespuesta.UsuarioModificacion));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", obj_pRespuesta.Estado));

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
        }
    }
}

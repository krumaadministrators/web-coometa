﻿using Kruma.Core.Data;
using Kruma.Core.Data.Entity;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Data
{
    public class PersonaDAO
    {
        /// <summary>Listar Persona</summary>
        /// <param name="int_pIdPersona">IdPersona</param>
        /// <param name="str_pNombresCompleto">NombresCompleto</param>
        /// <param name="str_pMail">Mail</param>
        /// <param name="int_pCelular">Celular</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de Persona</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<PersonaBE> ListarPersona(
            int? int_pIdPersona,
            string str_pNombresCompleto,
            string str_pMail,
            string str_pCelular,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            Kruma.Core.Util.Common.List<PersonaBE> obj_Lista = new Kruma.Core.Util.Common.List<PersonaBE>();
            obj_Lista.PageNumber = int_pNumPagina;
            obj_Lista.Total = 0;

            DataOperation dop_Operacion = new DataOperation("ListarCorePersona");
            dop_Operacion.Parameters.Add(new Parameter("@pIdPersona", int_pIdPersona.HasValue ? int_pIdPersona.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pNombresCompleto", !string.IsNullOrEmpty(str_pNombresCompleto) ? str_pNombresCompleto : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pMail", !string.IsNullOrEmpty(str_pMail) ? str_pMail : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pCelular", string.IsNullOrEmpty(str_pCelular) ? str_pCelular : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", !string.IsNullOrEmpty(str_pEstado) ? str_pEstado : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pNumPagina", int_pNumPagina.HasValue ? int_pNumPagina.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTamPagina", int_pTamPagina.HasValue ? int_pTamPagina.Value : (object)DBNull.Value));

            DataTable dt_Resultado = DataManager.ExecuteDataSet(Conexion.CO_SistemaFluVen, dop_Operacion).Tables[0];

            List<PersonaBE> lst_Persona = new List<PersonaBE>();
            PersonaBE obj_Persona = new PersonaBE();
            foreach (DataRow obj_Row in dt_Resultado.Rows)
            {
                if (lst_Persona.Count == 0)
                    obj_Lista.Total = (int)obj_Row["Total_Filas"];
                obj_Persona = new PersonaBE();
                obj_Persona.IdPersona = obj_Row["IdPersona"] is DBNull ? null : (int?)obj_Row["IdPersona"];
                obj_Persona.NombresCompleto = obj_Row["NombresCompleto"] is DBNull ? null : obj_Row["NombresCompleto"].ToString();
                obj_Persona.Correo = obj_Row["Mail"] is DBNull ? null : obj_Row["Mail"].ToString();
                obj_Persona.Celular = obj_Row["Celular"] is DBNull ? null : obj_Row["Celular"].ToString();
                obj_Persona.Estado = obj_Row["Estado"] is DBNull ? null : obj_Row["Estado"].ToString();
                obj_Persona.UsuarioCreacion = obj_Row["UsuarioCreacion"] is DBNull ? null : obj_Row["UsuarioCreacion"].ToString();
                obj_Persona.FechaCreacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaCreacion"] is DBNull ? null : (DateTime?)obj_Row["FechaCreacion"]);
                obj_Persona.UsuarioModificacion = obj_Row["UsuarioModificacion"] is DBNull ? null : obj_Row["UsuarioModificacion"].ToString();
                obj_Persona.FechaModificacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaModificacion"] is DBNull ? null : (DateTime?)obj_Row["FechaModificacion"]);

                lst_Persona.Add(obj_Persona);
            }

            obj_Lista.Result = lst_Persona;
            return obj_Lista;
        }

        /// <summary>Insertar Persona</summary>
        /// <param name="obj_pPersona">Persona</param>
        /// <returns>Id de Persona</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static int Insertar(PersonaBE obj_pPersona)
        {
            DataOperation dop_Operacion = new DataOperation("InsertarCorePersona");
            dop_Operacion.Parameters.Add(new Parameter("@pNombresCompleto", obj_pPersona.NombresCompleto));
            dop_Operacion.Parameters.Add(new Parameter("@pMail", obj_pPersona.Correo));
            dop_Operacion.Parameters.Add(new Parameter("@pCelular", obj_pPersona.Celular));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioCreacion", obj_pPersona.UsuarioCreacion));

            Parameter obj_IdPersona = new Parameter("@pIdPersona", DbType.Int32);
            obj_IdPersona.Direction = ParameterDirection.Output;
            dop_Operacion.Parameters.Add(obj_IdPersona);

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
            int int_IdPersona = (int)obj_IdPersona.Value;
            return int_IdPersona;
        }

        /// <summary>Actualizar Persona</summary>
        /// <param name="obj_pPersona">Persona</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static void Modificar(PersonaBE obj_pPersona)
        {
            DataOperation dop_Operacion = new DataOperation("ActualizarCorePersona");

            dop_Operacion.Parameters.Add(new Parameter("@pIdPersona", obj_pPersona.IdPersona));
            dop_Operacion.Parameters.Add(new Parameter("@pNombresCompleto", obj_pPersona.NombresCompleto));
            dop_Operacion.Parameters.Add(new Parameter("@pMail", obj_pPersona.Correo));
            dop_Operacion.Parameters.Add(new Parameter("@pCelular", obj_pPersona.Celular));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioModificacion", obj_pPersona.UsuarioModificacion));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", obj_pPersona.Estado));

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
        }
    }
}

﻿using Kruma.Core.Data;
using Kruma.Core.Data.Entity;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Data
{
    public class PreguntaDAO
    {

        /// <summary>Obtener Pregunta</summary>
        /// <param name="int_pIdPregunta">IdPregunta</param>
        /// <returns>Objeto Pregunta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static PreguntaBE Obtener(int? int_pIdPregunta)
        {
            Kruma.Core.Util.Common.List<PreguntaBE> lst_Pregunta = ListarPregunta(int_pIdPregunta, null,Kruma.FluVen.Entidad.Constante.Estado_Activo, null, null);
            return lst_Pregunta.Result.Count > 0 ? lst_Pregunta.Result[0] : null;
        }


        /// <summary>Listar Pregunta</summary>
        /// <param name="int_pIdPregunta">IdPregunta</param>
        /// <param name="str_pDescripcion">Descripcion</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de Pregunta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<PreguntaBE> ListarPregunta(
            int? int_pIdPregunta,
            string str_pDescripcion,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            Kruma.Core.Util.Common.List<PreguntaBE> obj_Lista = new Kruma.Core.Util.Common.List<PreguntaBE>();
            obj_Lista.PageNumber = int_pNumPagina;
            obj_Lista.Total = 0;

            DataOperation dop_Operacion = new DataOperation("ListarPregunta");
            dop_Operacion.Parameters.Add(new Parameter("@pIdPregunta", int_pIdPregunta.HasValue ? int_pIdPregunta.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pDescripcion", !string.IsNullOrEmpty(str_pDescripcion) ? str_pDescripcion : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", !string.IsNullOrEmpty(str_pEstado) ? str_pEstado : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pNumPagina", int_pNumPagina.HasValue ? int_pNumPagina.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTamPagina", int_pTamPagina.HasValue ? int_pTamPagina.Value : (object)DBNull.Value));

            DataTable dt_Resultado = DataManager.ExecuteDataSet(Conexion.CO_SistemaFluVen, dop_Operacion).Tables[0];

            List<PreguntaBE> lst_Pregunta = new List<PreguntaBE>();
            PreguntaBE obj_Pregunta = new PreguntaBE();
            foreach (DataRow obj_Row in dt_Resultado.Rows)
            {
                if (lst_Pregunta.Count == 0)
                    obj_Lista.Total = (int)obj_Row["Total_Filas"];
                obj_Pregunta = new PreguntaBE();
                obj_Pregunta.IdPregunta = obj_Row["IdPregunta"] is DBNull ? null : (int?)obj_Row["IdPregunta"];
                obj_Pregunta.Descripcion = obj_Row["Descripcion"] is DBNull ? null : obj_Row["Descripcion"].ToString();
                obj_Pregunta.DetallePregunta = obj_Row["DetallePregunta"] is DBNull ? null : obj_Row["DetallePregunta"].ToString();
                obj_Pregunta.Estado = obj_Row["Estado"] is DBNull ? null : obj_Row["Estado"].ToString();
                obj_Pregunta.UsuarioCreacion = obj_Row["UsuarioCreacion"] is DBNull ? null : obj_Row["UsuarioCreacion"].ToString();
                obj_Pregunta.FechaCreacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaCreacion"] is DBNull ? null : (DateTime?)obj_Row["FechaCreacion"]);
                obj_Pregunta.UsuarioModificacion = obj_Row["UsuarioModificacion"] is DBNull ? null : obj_Row["UsuarioModificacion"].ToString();
                obj_Pregunta.FechaModificacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaModificacion"] is DBNull ? null : (DateTime?)obj_Row["FechaModificacion"]);

                lst_Pregunta.Add(obj_Pregunta);
            }

            obj_Lista.Result = lst_Pregunta;
            return obj_Lista;
        }

        /// <summary>Insertar Pregunta</summary>
        /// <param name="obj_pPregunta">Pregunta</param>
        /// <returns>Id de Pregunta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static int Insertar(PreguntaBE obj_pPregunta)
        {
            DataOperation dop_Operacion = new DataOperation("InsertarPregunta");
            dop_Operacion.Parameters.Add(new Parameter("@pDescripcion", obj_pPregunta.Descripcion));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioCreacion", obj_pPregunta.UsuarioCreacion));

            Parameter obj_IdPregunta = new Parameter("@pIdPregunta", DbType.Int32);
            obj_IdPregunta.Direction = ParameterDirection.Output;
            dop_Operacion.Parameters.Add(obj_IdPregunta);

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
            int int_IdPregunta = (int)obj_IdPregunta.Value;
            return int_IdPregunta;
        }

        /// <summary>Actualizar Pregunta</summary>
        /// <param name="obj_pPregunta">Pregunta</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static void Modificar(PreguntaBE obj_pPregunta)
        {
            DataOperation dop_Operacion = new DataOperation("ActualizarPregunta");
            
            dop_Operacion.Parameters.Add(new Parameter("@pIdPregunta", obj_pPregunta.IdPregunta));
            dop_Operacion.Parameters.Add(new Parameter("@pDescripcion", obj_pPregunta.Descripcion));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioModificacion", obj_pPregunta.UsuarioModificacion));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", obj_pPregunta.Estado));

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
        }
    }
}


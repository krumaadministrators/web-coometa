﻿using Kruma.Core.Data;
using Kruma.Core.Data.Entity;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Data
{
    public class PreguntaOpcionDAO
    {
        /// <summary>Obtener PreguntaOpcion</summary>
        /// <param name="int_pIdPreguntaOpcion">IdPreguntaOpcion</param>
        /// <returns>Objeto PreguntaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static PreguntaOpcionBE Obtener(int? int_pIdPreguntaOpcion)
        {
            Kruma.Core.Util.Common.List<PreguntaOpcionBE> lst_PreguntaOpcion = ListarPreguntaOpcion(int_pIdPreguntaOpcion,null,null,null,null,Kruma.FluVen.Entidad.Constante.Estado_Activo, null, null);
            return lst_PreguntaOpcion.Result.Count > 0 ? lst_PreguntaOpcion.Result[0] : null;
        }

        /// <summary>Listar PreguntaOpcion</summary>
        /// <param name="int_pIdPreguntaOpcion">IdPregunta</param>
        /// <param name="int_pIdPregunta">IdPreguntaOpcion</param>
        /// <param name="str_pDescripcion">Descripcion</param>
        /// <param name="int_pCosto">Tiempo</param>
        /// <param name="int_pTiempo">Costo</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de PreguntaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<PreguntaOpcionBE> ListarPreguntaOpcion(
            int? int_pIdPreguntaOpcion,
            int? int_pIdPregunta,
            string str_pDescripcion,
            int? int_pTiempo,
            int? int_pCosto,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            Kruma.Core.Util.Common.List<PreguntaOpcionBE> obj_Lista = new Kruma.Core.Util.Common.List<PreguntaOpcionBE>();
            obj_Lista.PageNumber = int_pNumPagina;
            obj_Lista.Total = 0;

            DataOperation dop_Operacion = new DataOperation("ListarPreguntaOpcion");
            dop_Operacion.Parameters.Add(new Parameter("@pIdPreguntaOpcion", int_pIdPreguntaOpcion.HasValue ? int_pIdPreguntaOpcion.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pIdPregunta", int_pIdPregunta.HasValue ? int_pIdPregunta.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pDescripcion", !string.IsNullOrEmpty(str_pDescripcion) ? str_pDescripcion : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTiempo", int_pTiempo.HasValue ? int_pTiempo.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pCosto", int_pCosto.HasValue ? int_pCosto.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", !string.IsNullOrEmpty(str_pEstado) ? str_pEstado : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pNumPagina", int_pNumPagina.HasValue ? int_pNumPagina.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTamPagina", int_pTamPagina.HasValue ? int_pTamPagina.Value : (object)DBNull.Value));

            DataTable dt_Resultado = DataManager.ExecuteDataSet(Conexion.CO_SistemaFluVen, dop_Operacion).Tables[0];

            List<PreguntaOpcionBE> lst_PreguntaOpcion = new List<PreguntaOpcionBE>();
            PreguntaOpcionBE obj_PreguntaOpcion = new PreguntaOpcionBE();
            foreach (DataRow obj_Row in dt_Resultado.Rows)
            {
                if (lst_PreguntaOpcion.Count == 0)
                    obj_Lista.Total = (int)obj_Row["Total_Filas"];
                obj_PreguntaOpcion = new PreguntaOpcionBE();
                obj_PreguntaOpcion.IdPreguntaOpcion = obj_Row["IdPreguntaOpcion"] is DBNull ? null : (int?)obj_Row["IdPreguntaOpcion"];
                obj_PreguntaOpcion.IdPregunta = obj_Row["IdPregunta"] is DBNull ? null : (int?)obj_Row["IdPregunta"];
                obj_PreguntaOpcion.Descripcion = obj_Row["Descripcion"] is DBNull ? null : obj_Row["Descripcion"].ToString();
                obj_PreguntaOpcion.Tiempo = obj_Row["Tiempo"] is DBNull ? null : (Decimal?)obj_Row["Tiempo"];
                obj_PreguntaOpcion.Costo = obj_Row["Costo"] is DBNull ? null : (Decimal?)obj_Row["Costo"];
                obj_PreguntaOpcion.Imagen = obj_Row["Imagen"] is DBNull ? null : obj_Row["Imagen"].ToString();
                obj_PreguntaOpcion.Estado = obj_Row["Estado"] is DBNull ? null : obj_Row["Estado"].ToString();
                obj_PreguntaOpcion.TipoPregunta = obj_Row["TipoPregunta"] is DBNull ? null : obj_Row["TipoPregunta"].ToString();
                obj_PreguntaOpcion.UsuarioCreacion = obj_Row["UsuarioCreacion"] is DBNull ? null : obj_Row["UsuarioCreacion"].ToString();
                obj_PreguntaOpcion.FechaCreacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaCreacion"] is DBNull ? null : (DateTime?)obj_Row["FechaCreacion"]);
                obj_PreguntaOpcion.UsuarioModificacion = obj_Row["UsuarioModificacion"] is DBNull ? null : obj_Row["UsuarioModificacion"].ToString();
                obj_PreguntaOpcion.FechaModificacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaModificacion"] is DBNull ? null : (DateTime?)obj_Row["FechaModificacion"]);

                lst_PreguntaOpcion.Add(obj_PreguntaOpcion);
            }

            obj_Lista.Result = lst_PreguntaOpcion;
            return obj_Lista;
        }

        /// <summary>Insertar PreguntaOpcion</summary>
        /// <param name="obj_pPreguntaOpcion">PreguntaOpcion</param>
        /// <returns>Id de PreguntaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static int Insertar(PreguntaOpcionBE obj_pPreguntaOpcion)
        {
            DataOperation dop_Operacion = new DataOperation("InsertarPreguntaOpcion");
            dop_Operacion.Parameters.Add(new Parameter("@pIdPregunta", obj_pPreguntaOpcion.IdPregunta));
            dop_Operacion.Parameters.Add(new Parameter("@pDescripcion", obj_pPreguntaOpcion.Descripcion));
            dop_Operacion.Parameters.Add(new Parameter("@pTiempo", obj_pPreguntaOpcion.Tiempo));
            dop_Operacion.Parameters.Add(new Parameter("@pCosto", obj_pPreguntaOpcion.Costo));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioCreacion", obj_pPreguntaOpcion.UsuarioCreacion));

            Parameter obj_IdPreguntaOpcion = new Parameter("@pIdPreguntaOpcion", DbType.Int32);
            obj_IdPreguntaOpcion.Direction = ParameterDirection.Output;
            dop_Operacion.Parameters.Add(obj_IdPreguntaOpcion);

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
            int int_IdPreguntaOpcion = (int)obj_IdPreguntaOpcion.Value;
            return int_IdPreguntaOpcion;
        }

        /// <summary>Actualizar PreguntaOpcion</summary>
        /// <param name="obj_pPreguntaOpcion">PreguntaOpcion</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static void Modificar(PreguntaOpcionBE obj_pPreguntaOpcion)
        {
            DataOperation dop_Operacion = new DataOperation("ActualizarPreguntaOpcion");

            dop_Operacion.Parameters.Add(new Parameter("@pIdPreguntaOpcion", obj_pPreguntaOpcion.IdPreguntaOpcion));
            dop_Operacion.Parameters.Add(new Parameter("@pIdPregunta", obj_pPreguntaOpcion.IdPregunta));
            dop_Operacion.Parameters.Add(new Parameter("@pDescripcion", obj_pPreguntaOpcion.Descripcion));
            dop_Operacion.Parameters.Add(new Parameter("@pTiempo", obj_pPreguntaOpcion.Tiempo));
            dop_Operacion.Parameters.Add(new Parameter("@pCosto", obj_pPreguntaOpcion.Costo));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioModificacion", obj_pPreguntaOpcion.UsuarioModificacion));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", obj_pPreguntaOpcion.Estado));

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
        }
    }
}

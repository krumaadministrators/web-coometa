﻿using Kruma.Core.Data;
using Kruma.Core.Data.Entity;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Kruma.FluVen.Data
{
    public class RespuestaOpcionDAO
    {

        /// <summary>Listar RespuestaOpcion</summary>
        /// <param name="int_pIdRespuestaOpcion">IdRespuestaOpcion</param>
        /// <param name="int_pIdRespuesta">IdRespuesta</param>
        /// <param name="int_pIdPreguntaOpcion">IdPreguntaOpcion</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de RespuestaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>15-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<RespuestaOpcionBE> ListarRespuestaOpcion(
            int? int_pIdRespuestaOpcion,
            int? int_pIdRespuesta,
            int? int_pIdPreguntaOpcion,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            Kruma.Core.Util.Common.List<RespuestaOpcionBE> obj_Lista = new Kruma.Core.Util.Common.List<RespuestaOpcionBE>();
            obj_Lista.PageNumber = int_pNumPagina;
            obj_Lista.Total = 0;

            DataOperation dop_Operacion = new DataOperation("ListarRespuestaOpcion");
            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuestaOpcion", int_pIdRespuestaOpcion.HasValue ? int_pIdRespuestaOpcion.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuesta", int_pIdRespuesta.HasValue ? int_pIdRespuesta.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pIdPreguntaOpcion", int_pIdPreguntaOpcion.HasValue ? int_pIdPreguntaOpcion.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", !string.IsNullOrEmpty(str_pEstado) ? str_pEstado : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pNumPagina", int_pNumPagina.HasValue ? int_pNumPagina.Value : (object)DBNull.Value));
            dop_Operacion.Parameters.Add(new Parameter("@pTamPagina", int_pTamPagina.HasValue ? int_pTamPagina.Value : (object)DBNull.Value));

            DataTable dt_Resultado = DataManager.ExecuteDataSet(Conexion.CO_SistemaFluVen, dop_Operacion).Tables[0];

            List<RespuestaOpcionBE> lst_RespuestaOpcion = new List<RespuestaOpcionBE>();
            RespuestaOpcionBE obj_RespuestaOpcion = new RespuestaOpcionBE();
            foreach (DataRow obj_Row in dt_Resultado.Rows)
            {
                if (lst_RespuestaOpcion.Count == 0)
                    obj_Lista.Total = (int)obj_Row["Total_Filas"];
                obj_RespuestaOpcion = new RespuestaOpcionBE();
                obj_RespuestaOpcion.IdRespuestaOpcion = obj_Row["IdRespuestaOpcion"] is DBNull ? null : (int?)obj_Row["IdRespuestaOpcion"];
                obj_RespuestaOpcion.IdRespuesta = obj_Row["IdRespuesta"] is DBNull ? null : (int?)obj_Row["IdRespuesta"];
                obj_RespuestaOpcion.IdPreguntaOpcion = obj_Row["IdPreguntaOpcion"] is DBNull ? null : (int?)obj_Row["IdPreguntaOpcion"];
                obj_RespuestaOpcion.Observacion = obj_Row["Observacion"] is DBNull ? null : obj_Row["Observacion"].ToString();
                obj_RespuestaOpcion.Estado = obj_Row["Estado"] is DBNull ? null : obj_Row["Estado"].ToString();
                obj_RespuestaOpcion.UsuarioCreacion = obj_Row["UsuarioCreacion"] is DBNull ? null : obj_Row["UsuarioCreacion"].ToString();
                obj_RespuestaOpcion.FechaCreacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaCreacion"] is DBNull ? null : (DateTime?)obj_Row["FechaCreacion"]);
                obj_RespuestaOpcion.UsuarioModificacion = obj_Row["UsuarioModificacion"] is DBNull ? null : obj_Row["UsuarioModificacion"].ToString();
                obj_RespuestaOpcion.FechaModificacion = Kruma.Core.Util.DateTimeUtil.UtcToDateTime(obj_Row["FechaModificacion"] is DBNull ? null : (DateTime?)obj_Row["FechaModificacion"]);

                lst_RespuestaOpcion.Add(obj_RespuestaOpcion);
            }

            obj_Lista.Result = lst_RespuestaOpcion;
            return obj_Lista;
        }

        /// <summary>Insertar RespuestaOpcion</summary>
        /// <param name="obj_pRespuestaOpcion">RespuestaOpcion</param>
        /// <returns>Id de RespuestaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static int Insertar(RespuestaOpcionBE obj_pRespuestaOpcion)
        {
            DataOperation dop_Operacion = new DataOperation("InsertarRespuestaOpcion");
            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuesta", obj_pRespuestaOpcion.IdRespuesta));
            dop_Operacion.Parameters.Add(new Parameter("@pIdPreguntaOpcion", obj_pRespuestaOpcion.IdPreguntaOpcion));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioCreacion", obj_pRespuestaOpcion.UsuarioCreacion));
            dop_Operacion.Parameters.Add(new Parameter("@pObservacion", obj_pRespuestaOpcion.Observacion));
          
            Parameter obj_IdRespuestaOpcion = new Parameter("@pIdRespuestaOpcion", DbType.Int32);
            obj_IdRespuestaOpcion.Direction = ParameterDirection.Output;
            dop_Operacion.Parameters.Add(obj_IdRespuestaOpcion);

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
            int int_IdRespuestaOpcion = (int)obj_IdRespuestaOpcion.Value;
            return int_IdRespuestaOpcion;
        }

        /// <summary>Actualizar RespuestaOpcion</summary>
        /// <param name="obj_pRespuestaOpcion">RespuestaOpcion</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static void Modificar(RespuestaOpcionBE obj_pRespuestaOpcion)
        {
            DataOperation dop_Operacion = new DataOperation("ActualizarRespuestaOpcion");

            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuestaOpcion", obj_pRespuestaOpcion.IdRespuestaOpcion));
            dop_Operacion.Parameters.Add(new Parameter("@pIdRespuesta", obj_pRespuestaOpcion.IdRespuesta));
            dop_Operacion.Parameters.Add(new Parameter("@pIdPreguntaOpcion", obj_pRespuestaOpcion.IdPreguntaOpcion));
            dop_Operacion.Parameters.Add(new Parameter("@pUsuarioModificacion", obj_pRespuestaOpcion.UsuarioModificacion));
            dop_Operacion.Parameters.Add(new Parameter("@pEstado", obj_pRespuestaOpcion.Estado));

            DataManager.ExecuteNonQuery(Conexion.CO_SistemaFluVen, dop_Operacion, false);
        }
    }
}

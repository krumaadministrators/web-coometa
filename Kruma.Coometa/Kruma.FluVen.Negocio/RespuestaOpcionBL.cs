﻿using Kruma.Core.Util.Common;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Kruma.FluVen.Negocio
{
    public class RespuestaOpcionBL
    {
        /// <summary>Listar RespuestaOpcion</summary>
        /// <param name="int_pIdRespuestaOpcion">IdRespuestaOpcion</param>
        /// <param name="int_pIdRespuesta">IdRespuesta</param>
        /// <param name="int_pIdPreguntaOpcion">IdPreguntaOpcion</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de RespuestaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>15-07-2020</FecCrea></item></list></remarks>
        public Kruma.Core.Util.Common.List<RespuestaOpcionBE> ListarRespuestaOpcion(
            int? int_pIdRespuestaOpcion,
            int? int_pIdRespuesta,
            int? int_pIdPreguntaOpcion,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            return Data.RespuestaOpcionDAO.ListarRespuestaOpcion(int_pIdRespuestaOpcion, int_pIdRespuesta, int_pIdPreguntaOpcion, str_pEstado, int_pNumPagina, int_pTamPagina);
        }

        /// <summary>Insertar RespuestaOpcion</summary>
        /// <param name="obj_pRespuestaOpcion">RespuestaOpcion</param>
        /// <returns>Id de RespuestaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Insertar(RespuestaOpcionBE obj_pRespuestaOpcion)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    obj_pRespuestaOpcion.IdRespuestaOpcion = Data.RespuestaOpcionDAO.Insertar(obj_pRespuestaOpcion);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pRespuestaOpcion.IdRespuestaOpcion);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }

        /// <summary>Actualizar RespuestaOpcion</summary>
        /// <param name="obj_pRespuestaOpcion">RespuestaOpcion</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Modificar(RespuestaOpcionBE obj_pRespuestaOpcion)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    Data.RespuestaOpcionDAO.Modificar(obj_pRespuestaOpcion);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pRespuestaOpcion.IdRespuestaOpcion);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }
    }
}

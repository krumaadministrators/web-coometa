﻿using Kruma.Core.Util.Common;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Kruma.FluVen.Negocio
{
    public class RespuestaBL
    {
        /// <summary>Listar Respuesta</summary>
        /// <param name="int_pIdRespuesta">IdRespuesta</param>
        /// <param name="int_pIdPersona">IdPersona</param>
        /// <param name="str_pMensaje">Mensaje</param>
        /// <param name="int_pTiempo">Tiempo</param>
        /// <param name="int_pCosto">Costo</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de Respuesta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public Kruma.Core.Util.Common.List<RespuestaBE> ListarRespuesta(
            int? int_pIdRespuesta,
            int? int_pIdPersona,
            string str_pMensaje,
            int? int_pTiempo,
            int? int_pCosto,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            return Data.RespuestaDAO.ListarRespuesta(int_pIdRespuesta, int_pIdPersona, str_pMensaje, int_pTiempo, int_pCosto, str_pEstado, int_pNumPagina, int_pTamPagina);
        }

        /// <summary>Insertar Respuesta</summary>
        /// <param name="obj_pRespuesta">Respuesta</param>
        /// <returns>Id de Respuesta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Insertar(RespuestaBE obj_pRespuesta)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    obj_pRespuesta.IdRespuesta = Data.RespuestaDAO.Insertar(obj_pRespuesta);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pRespuesta.IdRespuesta);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }

        /// <summary>Insertar Respuesta</summary>
        /// <param name="obj_pRespuesta">Respuesta</param>
        /// <returns>Id de Respuesta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult InsertarClienteRespuesta(RegistroRespuesta obj_pRespuesta)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {
                    PersonaBE persona = new PersonaBE();
                    persona.NombresCompleto = obj_pRespuesta.RegistroRespuestaDetalle[0].NombreCompleto;
                    persona.Correo = obj_pRespuesta.RegistroRespuestaDetalle[0].Correo;
                    persona.Celular = obj_pRespuesta.RegistroRespuestaDetalle[0].Celular;
                    int IdPersona = Data.PersonaDAO.Insertar(persona);

                    Decimal Tiempo = 1;
                    Decimal Costo = 0;
                    /* foreach (var item in obj_pRespuesta.RegistroRespuestaDetalle)
                     {
                         PreguntaOpcionBE obj_PreguntaOpcion = Data.PreguntaOpcionDAO.Obtener(item.IdPreguntaOpcion);
                         if (obj_PreguntaOpcion.Tiempo != null)
                         {
                             Tiempo = (obj_PreguntaOpcion.Tiempo.Value * Tiempo);
                         }
                         if (obj_PreguntaOpcion.Costo != null)
                         {
                             Costo += obj_PreguntaOpcion.Costo.Value;
                         }

                     }
                     */
                    RespuestaBE respuesta = new RespuestaBE();
                    respuesta.IdPersona = IdPersona;
                    respuesta.Mensaje = obj_pRespuesta.RegistroRespuestaDetalle[0].Mensaje;


                    //respuesta.Tiempo = Math.Round(Tiempo, MidpointRounding.AwayFromZero);
                    //respuesta.Costo = Costo* respuesta.Tiempo;
                    int IdRespuesta = Data.RespuestaDAO.Insertar(respuesta);
                    int IdRespuestaOpcion = 0;
                    //Respuesta del usuario
                    StringBuilder stbRespuestaPregunta = new StringBuilder();
                    //StringBuilder stbRespuestaOpcion = new StringBuilder();
                    string respuestaPreguntaComple = "";
                    int contador = 0;
                    //string respuestaOpcionComple = "";
                    foreach (var item in obj_pRespuesta.RegistroRespuestaDetalle)
                    {
                        RespuestaOpcionBE respuestaOpcion = new RespuestaOpcionBE();
                        respuestaOpcion.IdPreguntaOpcion = item.IdPreguntaOpcion;
                        respuestaOpcion.IdRespuesta = IdRespuesta;
                        respuestaOpcion.Observacion = item.Observacion;
                        IdRespuestaOpcion = Data.RespuestaOpcionDAO.Insertar(respuestaOpcion);
                        contador++;
                        PreguntaOpcionBE respuestaUsuario = Data.PreguntaOpcionDAO.Obtener(respuestaOpcion.IdPreguntaOpcion);
                        PreguntaBE preguntaUsuario = Data.PreguntaDAO.Obtener(respuestaUsuario.IdPregunta);
                        if (respuestaUsuario.IdPregunta < 9)
                            respuestaPreguntaComple = string.Format("<b>" + contador + ".- {0}</b><br> - {1}<br>", preguntaUsuario.Descripcion, respuestaUsuario.Descripcion + " " + item.Observacion);
                        //respuestaOpcionComple = string.Format("{0}", respuestaUsuario.Descripcion);
                        stbRespuestaPregunta.Append(respuestaPreguntaComple);
                        //stbRespuestaOpcion.Append(respuestaOpcionComple);
                    }

                    //Sed Email
                    Kruma.Core.Notification.Entity.MailEntry obj_MailEntry = new Kruma.Core.Notification.Entity.MailEntry();
                    obj_MailEntry.To = obj_pRespuesta.RegistroRespuestaDetalle[0].Correo;
                    //obj_MailEntry.From = "cae@kruma.pe";//System.Configuration.ConfigurationManager.AppSettings.Get("Emisor");
                    obj_MailEntry.Subject = "Búsqueda de Mentor";
                    obj_MailEntry.IdTipoCorreo = Kruma.Core.Notification.Enum.MailType.Template;
                    obj_MailEntry.BodyFileName = string.Format("{0}{1}",
                    System.Web.HttpContext.Current.Server.MapPath("~"),
                    "\\Plantillas\\Correo\\Busqueda.htm");
                    obj_MailEntry.isHTML = true;
                    //Mensaje del correo
                    StringBuilder stbMensaje = new StringBuilder();
                    StringBuilder stbMensajeT = new StringBuilder();
                    StringBuilder stbMensajeC = new StringBuilder();
                    StringBuilder stbMensajeMail = new StringBuilder();
                    //<b><%PREGUNTA%></b> <%RESPUESTA%> 
                    obj_MailEntry.Replacements.Add("<%PREGUNTA%>", stbRespuestaPregunta);
                    //obj_MailEntry.Replacements.Add("<%RESPUESTA%>", stbRespuestaOpcion);
                    //
                    obj_MailEntry.Replacements.Add("<%NOMBRES%>", persona.NombresCompleto);
                    //stbMensajeT.Append(double.Parse(respuesta.Tiempo.Value.ToString()));
                    //stbMensajeT.Append(string.Format("{0}{1}", respuesta.Tiempo.Value.ToString(), " meses "));
                    //
                    obj_MailEntry.Replacements.Add("<%CELULAR%>", persona.Celular);

                    obj_MailEntry.Replacements.Add("<%MAIL%>", persona.Correo);
                    // stbMensaje.Append(string.Format("<br><br>Atentamente<br><br>{0}", obj_ParametroSistemaNombre.Valor));
                    //int costoFinal = decimal.ToInt32(respuesta.Costo.Value);
                    //stbMensajeC.Append(string.Format("{0:n0}", costoFinal)); 
                    //obj_MailEntry.Replacements.Add("<%COSTO%>", stbMensajeC);
                    /* string urlimgCarlos = string.Format("{0}{1}",System.Web.HttpContext.Current.Server.MapPath("~"),"\\Plantillas\\Correo\\Firma.png");
                    if (!string.IsNullOrEmpty(urlimgCarlos))
                    {
                        LinkedResource imgCarlos = new LinkedResource(urlimgCarlos, "image/png");
                        obj_MailEntry.LinkedResources = new Dictionary<string, LinkedResource>();
                        obj_MailEntry.LinkedResources.Add("imgCarlos", imgCarlos);
                        stbMensaje.Append("<img width='70px;' src=cid:imgCarlos />");
                        obj_MailEntry.Replacements.Add("<%FIRMA%>", stbMensaje.ToString());
                    }
                    
                    StringBuilder stbMensajelogo = new StringBuilder();
                    string urlimgLogo = string.Format("{0}{1}", System.Web.HttpContext.Current.Server.MapPath("~"), "\\Plantillas\\Correo\\Kruma.png");
                    if (!string.IsNullOrEmpty(urlimgLogo))
                    {
                        LinkedResource imgLogo = new LinkedResource(urlimgLogo, "image/png");
                        obj_MailEntry.LinkedResources.Add("imgLogo", imgLogo);
                        stbMensajelogo.Append("<img width='70px;' src=cid:imgLogo />");
                        obj_MailEntry.Replacements.Add("<%FIRMALOGO%>", stbMensajelogo.ToString());
                    }

                   */
                    Kruma.Core.Notification.NotificationManager obj_NotificationManager = new Kruma.Core.Notification.NotificationManager("Default");
                    obj_NotificationManager.Send(obj_MailEntry);

                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(IdRespuestaOpcion);


                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }

        /// <summary>Actualizar Respuesta</summary>
        /// <param name="obj_pRespuesta">Respuesta</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Modificar(RespuestaBE obj_pRespuesta)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    Data.RespuestaDAO.Modificar(obj_pRespuesta);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pRespuesta.IdRespuesta);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }
    }
}
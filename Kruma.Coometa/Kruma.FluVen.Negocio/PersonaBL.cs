﻿using Kruma.Core.Util.Common;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Transactions;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Negocio
{
    public class PersonaBL
    {
        /// <summary>Listar Persona</summary>
        /// <param name="int_pIdPersona">IdPersona</param>
        /// <param name="str_pNombresCompleto">NombresCompleto</param>
        /// <param name="str_pMail">Mail</param>
        /// <param name="str_pCelular">Celular</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de Persona</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<PersonaBE> ListarPersona(
            int? int_pIdPersona,
            string str_pNombresCompleto,
            string str_pMail,
            string str_pCelular,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {

            return Data.PersonaDAO.ListarPersona(int_pIdPersona, str_pNombresCompleto, str_pMail, str_pCelular, str_pEstado, int_pNumPagina, int_pTamPagina);
        }

        /// <summary>Insertar Persona</summary>
        /// <param name="obj_pPersona">Persona</param>
        /// <returns>Id de Persona</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Insertar(PersonaBE obj_pPersona)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    obj_pPersona.IdPersona = Data.PersonaDAO.Insertar(obj_pPersona);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pPersona.IdPersona);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }

        /// <summary>Actualizar Persona</summary>
        /// <param name="obj_pPersona">Persona</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Modificar(PersonaBE obj_pPersona)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    Data.PersonaDAO.Modificar(obj_pPersona);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pPersona.IdPersona);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }
    }
}

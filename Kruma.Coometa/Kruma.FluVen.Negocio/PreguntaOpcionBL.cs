﻿using Kruma.Core.Util.Common;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Kruma.FluVen.Negocio
{
    public class PreguntaOpcionBL
    {
        /// <summary>Listar PreguntaOpcion</summary>
        /// <param name="int_pIdPreguntaOpcion">IdPregunta</param>
        /// <param name="int_pIdPregunta">IdPreguntaOpcion</param>
        /// <param name="str_pDescripcion">Descripcion</param>
        /// <param name="int_pCosto">Tiempo</param>
        /// <param name="int_pTiempo">Costo</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de PreguntaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<PreguntaOpcionBE> ListarPreguntaOpcion(
            int? int_pIdPreguntaOpcion,
            int? int_pIdPregunta,
            string str_pDescripcion,
            int? int_pTiempo,
            int? int_pCosto,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            return Data.PreguntaOpcionDAO.ListarPreguntaOpcion(int_pIdPreguntaOpcion, int_pIdPregunta, str_pDescripcion, int_pTiempo, int_pCosto, str_pEstado, int_pNumPagina, int_pTamPagina);
        }

        /// <summary>Insertar PreguntaOpcion</summary>
        /// <param name="obj_pPreguntaOpcion">PreguntaOpcion</param>
        /// <returns>Id de PreguntaOpcion</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Insertar(PreguntaOpcionBE obj_pPreguntaOpcion)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    obj_pPreguntaOpcion.IdPreguntaOpcion = Data.PreguntaOpcionDAO.Insertar(obj_pPreguntaOpcion);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pPreguntaOpcion.IdPreguntaOpcion);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }

        /// <summary>Actualizar PreguntaOpcion</summary>
        /// <param name="obj_pPreguntaOpcion">PreguntaOpcion</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Modificar(PreguntaOpcionBE obj_pPreguntaOpcion)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    Data.PreguntaOpcionDAO.Modificar(obj_pPreguntaOpcion);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pPreguntaOpcion.IdPreguntaOpcion);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }
    }
}

﻿using Kruma.Core.Util.Common;
using Kruma.FluVen.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Kruma.FluVen.Negocio
{
    public class PreguntaBL
    {
        /// <summary>Listar Pregunta</summary>
        /// <param name="int_pIdPregunta">IdPregunta</param>
        /// <param name="str_pDescripcion">Descripcion</param>
        /// <param name="str_pEstado">Estado</param>
        /// <param name="int_pNumPagina" >Numero de pagina</param>
        /// <param name="int_pTamPagina" >Tamaño de pagina</param>
        /// <returns>Lista de Pregunta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static Kruma.Core.Util.Common.List<PreguntaBE> ListarPregunta(
            int? int_pIdPregunta,
            string str_pDescripcion,
            string str_pEstado,
            int? int_pNumPagina,
            int? int_pTamPagina
            )
        {
            return Data.PreguntaDAO.ListarPregunta(int_pIdPregunta, str_pDescripcion, str_pEstado, int_pNumPagina, int_pTamPagina);
        }

        /// <summary>Insertar Pregunta</summary>
        /// <param name="obj_pPregunta">Pregunta</param>
        /// <returns>Id de Pregunta</returns>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Insertar(PreguntaBE obj_pPregunta)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    obj_pPregunta.IdPregunta = Data.PreguntaDAO.Insertar(obj_pPregunta);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pPregunta.IdPregunta);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }

        /// <summary>Actualizar Pregunta</summary>
        /// <param name="obj_pPregunta">Pregunta</param>
        /// <remarks><list type="bullet">
        /// <item><CreadoPor>Creado por Jaqueline DiazB</CreadoPor></item>
        /// <item><FecCrea>16-07-2020</FecCrea></item></list></remarks>
        public static ProcessResult Modificar(PreguntaBE obj_pPregunta)
        {
            ProcessResult obj_Resultado = null;
            try
            {
                using (TransactionScope obj_TransactionScope = new TransactionScope())
                {

                    Data.PreguntaDAO.Modificar(obj_pPregunta);
                    obj_TransactionScope.Complete();
                    obj_Resultado = new ProcessResult(obj_pPregunta.IdPregunta);
                }
            }
            catch (Exception obj_pExcepcion)
            {
                obj_Resultado = new ProcessResult(obj_pExcepcion);
            }
            return obj_Resultado;
        }
    }
}

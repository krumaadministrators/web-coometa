﻿using System;
using System.Runtime.Serialization;

namespace Kruma.FluVen.Entidad
{
    [Serializable]
    public class RespuestaOpcionBE
    {
        [DataMember]
        public int? IdRespuestaOpcion { get; set; }
        [DataMember]
        public int? IdRespuesta { get; set; }
        [DataMember]
        public int? IdPreguntaOpcion { get; set; }
        [DataMember]
        public string Observacion { get; set; }
        [DataMember]
        public string UsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string UsuarioModificacion { get; set; }
        [DataMember]
        public DateTime? FechaModificacion { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
}

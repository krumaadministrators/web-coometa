﻿using System;
using System.Runtime.Serialization;

namespace Kruma.FluVen.Entidad
{
    [Serializable]
    public class PreguntaBE
    {
        [DataMember]
        public int? IdPregunta { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string DetallePregunta { get; set; }
        [DataMember]
        public string UsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string UsuarioModificacion { get; set; }
        [DataMember]
        public DateTime? FechaModificacion { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Entidad
{
    [Serializable]
    public class RegistroRespuesta
    {
        [DataMember]
        public string NombreCompleto { get; set; }
        [DataMember]
        public string Celular { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string Mensaje { get; set; }
        [DataMember]
        public int? IdPreguntaOpcion { get; set; }
        [DataMember]
        public int? IdPregunta { get; set; }
        [DataMember]
        public string Observacion { get; set; }
        [DataMember]
        public string UsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string UsuarioModificacion { get; set; }
        [DataMember]
        public DateTime? FechaModificacion { get; set; }

        [DataMember]
        public List<Kruma.FluVen.Entidad.RegistroRespuesta> RegistroRespuestaDetalle { get; set; }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace Kruma.FluVen.Entidad
{
    [Serializable]
    public class RespuestaBE
    {
        [DataMember]
        public int? IdRespuesta { get; set; }
        [DataMember]
        public int? IdPersona { get; set; }
        [DataMember]
        public string Mensaje { get; set; }
        [DataMember]
        public Decimal? Tiempo { get; set; }
        [DataMember]
        public Decimal? Costo { get; set; }
        [DataMember]
        public string UsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string UsuarioModificacion { get; set; }
        [DataMember]
        public DateTime? FechaModificacion { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
}

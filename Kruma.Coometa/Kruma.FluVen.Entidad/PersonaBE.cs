﻿using System;
using System.Runtime.Serialization;

namespace Kruma.FluVen.Entidad
{
    [Serializable]
    public class PersonaBE
    {
        [DataMember]
        public int? IdPersona { get; set; }
        [DataMember]
        public string NombresCompleto { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string Celular { get; set; }
        [DataMember]
        public string UsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string UsuarioModificacion { get; set; }
        [DataMember]
        public DateTime? FechaModificacion { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruma.FluVen.Entidad
{
    public class Constante
    {
        public const string Estado_Activo = "A";
        public const string Estado_Inactivo = "I";
        public const string Modulo_FluVen = "FLUVEN";
        public const string IdParametro_Costo = "COSTO";
        public const string Emprendimiento_Costo = "EMPRENDIMIENTO";
        public const string Mype_Costo = "MYPE";
        public const string Mediana_Costo = "MEDIANA";
        public const string Tiempo_General = "TIEMPO";
    }
}

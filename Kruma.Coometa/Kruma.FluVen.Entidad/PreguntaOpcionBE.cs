﻿using System;
using System.Runtime.Serialization;

namespace Kruma.FluVen.Entidad
{
    [Serializable]
    public class PreguntaOpcionBE
    {
        [DataMember]
        public int? IdPreguntaOpcion { get; set; }
        [DataMember]
        public int? IdPregunta { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public Decimal? Tiempo { get; set; }
        [DataMember]
        public Decimal? Costo { get; set; }
        [DataMember]
        public string UsuarioCreacion { get; set; }
        [DataMember]
        public string Imagen { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string UsuarioModificacion { get; set; }
        [DataMember]
        public DateTime? FechaModificacion { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string TipoPregunta { get; set; }
    }
}

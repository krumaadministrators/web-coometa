﻿using Kruma.Core.Util.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Script.Serialization;
using System.Web.Services;

public partial class _Default : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {

    }

    [WebMethod]
    public static object ListarPregunta()
    {
        return Kruma.FluVen.Negocio.PreguntaBL.ListarPregunta(null, null,Kruma.FluVen.Entidad.Constante.Estado_Activo,null,null);
    }

    [WebMethod]
    public static object ListarOpcionxPregunta(int int_IdPregunta)
    {
        return Kruma.FluVen.Negocio.PreguntaOpcionBL.ListarPreguntaOpcion(null, int_IdPregunta,null,null,null, Kruma.FluVen.Entidad.Constante.Estado_Activo, null, null);
    }

    [WebMethod]
    public static Kruma.Core.Util.Common.ProcessResult GuardarCliente(string str_pCliente)
    {
        JavaScriptSerializer obj_JsSerializer = new JavaScriptSerializer();
        Kruma.FluVen.Entidad.RegistroRespuesta obj_Cliente = obj_JsSerializer.Deserialize<Kruma.FluVen.Entidad.RegistroRespuesta>(str_pCliente);
        obj_Cliente.UsuarioCreacion = "CLIENTE";
        obj_Cliente.UsuarioModificacion = "CLIENTE";
        Kruma.Core.Util.Common.ProcessResult obj_Resultado = null;
        obj_Resultado = Kruma.FluVen.Negocio.RespuestaBL.InsertarClienteRespuesta(obj_Cliente);
        return obj_Resultado;
    }

}

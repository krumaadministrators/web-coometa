﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PoliticaManejoDatos.aspx.cs" Inherits="PoliticaManejoDatos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Encuentra tu mentor - Coometa</title>
    <link rel='stylesheet' href="Style/bootstrap-4.0.0/dist/css/bootstrap.css" />
    <link rel='stylesheet' href="Style/Index.css" />
    <link href="~/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="card text-center" style="width: 80%; margin: 0 auto;">
      <div class="card-header" style="background-color: #373838;">
       <h2 class="card-title" style="font-weight:bold; color:white;">CARTA DE AUTORIZACIÓN PARA AFILIACIÓN Y TRATAMIENTO DE DATOS PERSONALES EN LA PLATAFORMA DE COOMETA</h2>
      </div>
      <div class="card-body" style="text-align:justify">
        
        <p class="card-text">
            Quien suscribe el presente documento, autoriza y otorga su consentimiento expreso a la Empresa ESPACIO DE IDEAS S.A.C. identificada con RUC N°20602608701, en adelante COOMETA, a fin de que como propietarias de la plataforma “Encuentra tu mentor”, en adelante LA PLATAFORMA, puedan afiliarme y dar tratamiento a mis datos personales, mismos que figuran en el presente documento.

Inclusive si con ocasión de mi afiliación, COOMETA decide realizar llamadas, envío de mensajes u otra comunicación, a los números que he brindado de forma libre, voluntaria y gratuita.

Asimismo, en fiel cumplimiento de la Ley N° 29733, “Ley de Protección de Datos Personales” y su Reglamento aprobado por Decreto Supremo N° 003-2013-JUS, autorizo y doy a COOMETA mi consentimiento libre, expreso, inequívoco e informado para almacenar -de forma indefinida- en sus bases de datos personales, los datos proporcionados líneas debajo, así como de toda la información derivada como resultado del uso de LA PLATAFORMA.

Finalmente, declaro conocer que, conforme a Ley, podré ejercer los derechos de información, acceso, actualización, inclusión, rectificación, supresión y oposición sobre sus datos personales, enviando una comunicación a COOMETA.

        </p>
       
      </div>
      <div class="card-footer text-muted"  style="background-color: #373838;">
      
      </div>
    </div>
    </form>
</body>
</html>

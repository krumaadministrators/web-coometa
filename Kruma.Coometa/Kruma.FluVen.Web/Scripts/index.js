﻿var int_gCantidadPreguntas = 0;
var int_gContadorClicks = 0;
var lst_gPreguntas = [];
var lst_gOpcionesSelecc = [];
var FlagContinuar = false;
$(function () {
    CargarInicial();
    //CargarControles();
});

function CargarInicial() {
    $('#chkPolitica2').change(function () {
        var value = "";
        if ($(this).is(":checked")) {
            value = 1;
            document.getElementById("chkPolitica").value = value;
            //document.getElementById("myDIV").style.display = "none";
            document.getElementById("chkPolitica-error").style.display = "none";
        } else {
            document.getElementById("chkPolitica").value = value;
            document.getElementById("chkPolitica-error").style.display = "block";
        }

    });

    //
    FlagContinuar = false
    $("#btnRegresar").hide();
    $("#btnContinuar").hide();
    $("#btnEnviar").hide();
    $("#IdFormulario").hide();
    $("#IdCarMensaje").hide();
    $("#btnRegresarFin").hide();
    //$("#IdDetallePregunta").hide();
    // $("#IdDetallePreguntaRes").hide();
    $("#btnVolverCotizar").click(function () {
        location.reload();
    })

    ListarPreguntas();
    $(".btnRegresar").click(function () {
        //var id1 = "#IDLi" + int_gContadorClicks;
        //$(id1).removeClass("activo");
        $("#IdFormulario").hide();
        $("#IdPanelTitle").show();
        $("#btnEnviar").hide();
        $("#btnContinuar").hide();
        $("#btnRegresarFin").hide();
        obj_OpcionSelec = null;
        FlagContinuar = false;
        int_gContadorClicks--;
        if (int_gContadorClicks >= 0) {
            //$("#IdDetallePregunta").text(lst_gPreguntas[int_gContadorClicks].DetallePregunta);
            //$("#IdDetallePreguntaRes").text(lst_gPreguntas[0].DetallePregunta);
            $('#IdConsulta').tooltip('dispose');
            if (lst_gPreguntas[int_gContadorClicks].DetallePregunta != null) {
                $('#IdConsulta').tooltip({ 'title': lst_gPreguntas[int_gContadorClicks].DetallePregunta });
                $("#txtPregunta").text(lst_gPreguntas[int_gContadorClicks].Descripcion);
            } else {
                $('#IdConsulta').tooltip({ 'title': '' });
            }
            $("#IdPregunta").val("");
            $("#IdPregunta").val(lst_gPreguntas[int_gContadorClicks].IdPregunta);
            lst_gOpcionesSelecc = jQuery.grep(lst_gOpcionesSelecc, function (obj_Detalle) {
                return obj_Detalle.IdPregunta != lst_gPreguntas[int_gContadorClicks].IdPregunta;
            });
            $(".opciones").empty();
            //var id = "#IDLi" + int_gContadorClicks;
            //$(id).addClass("activo");
            CargarOpciones(lst_gPreguntas[int_gContadorClicks].IdPregunta);
            if (int_gContadorClicks == 0) {
                $("#btnRegresar").hide();
                $("#btnContinuar").hide();
                $("#btnRegresarFin").hide();
                $("#btnEnviar").hide();
                //$("IDLi0").addClass("activo");
            }
        } else {
            $("#btnRegresarFin").hide();
            $("#btnRegresar").hide();
            $("#btnContinuar").hide();
            $("#btnEnviar").hide();
        }


    })
    function ValidarCliente() {
        var settings = $('#frmContent').validate().settings;
        $('.form-group').removeClass('has-error').removeClass('has-success');
        $.extend(settings, {
            ignore: "",
            rules: {
                txtNombreC: { required: true },
                txtMail: { required: true, email: true },
                txtCelular: { required: true },
                chkPolitica: { required: true }
            },
            messages: {
                txtNombreC: { required: 'Ingrese su nombre' },
                txtMail: { required: 'Ingrese su correo', email: "Ingrese un correo valido" },
                txtCelular: { required: 'Ingrese su número de celular' },
                chkPolitica: { required: 'Debe aceptar las políticas de privacidad.' },
            },

        });

    }

    $("#btnEnviar").click(function () {

        ValidarCliente();
        var valor = $('#frmContent').valid();
        if ($('#frmContent').valid()) {
            var lst_ListadoGuardar = [];
            var obj_Cliente = null;
            for (var i = 0; i < lst_gOpcionesSelecc.length; i++) {
                obj_Cliente = new Object();
                obj_Cliente.NombreCompleto = $("#txtNombreC").val();
                obj_Cliente.Celular = $("#txtCelular").val()
                obj_Cliente.Correo = $('#txtMail').val(),
                obj_Cliente.Celular = $("#txtCelular").val(),
                obj_Cliente.Mensaje = $("#txtGiroN").val();
                obj_Cliente.IdPreguntaOpcion = lst_gOpcionesSelecc[i].IdPreguntaOpcion;
                obj_Cliente.IdPregunta = lst_gOpcionesSelecc[i].IdPregunta;
                obj_Cliente.Observacion = lst_gOpcionesSelecc[i].Observacion;
                lst_ListadoGuardar.push(obj_Cliente);
            }
            if (lst_ListadoGuardar.length > 0) {
                var listado = new Object();
                listado.RegistroRespuestaDetalle = lst_ListadoGuardar;
                Accion('Default.aspx/GuardarCliente', { "str_pCliente": JSON.stringify(listado) },
                function (obj_Result) {
                    //AlertJQ(1, 'Se le envio un correo con la cotizacion. Gracias.');
                    $("#IdCarMensaje").show();
                    $("#IdFormulario").hide();
                    $("#IdBotones").hide();
                    /*setTimeout(function () {
                        document.location.href = "Default.aspx";
                    }, 4000);*/
                });
            } else {
                //AlertJQ(3, 'El numero de serie debe ser mayor a 0');
            }
        }
        return false;
    })

    $("#btnContinuar").click(function () {
        if ( $("#txtAreaRespuesta").val().length > 0) {
            FlagContinuar = true;
            CambiarPregunta(obj_OpcionSelec);
        }
        else {
            $("#txtAreaRespuesta").focus();
        }
    })
}

function CambiarPregunta(pPreguntaOpcion) {
    // $("#IdDetallePregunta").hide();
    //$("#IdDetallePreguntaRes").hide();

    var IdOpcion = 0;
    var OpcionSeleccionada = null;

    if (!FlagContinuar) {
        $(this).find('input').each(function () {
            IdOpcion = $(this).val();
        });
        OpcionSeleccionada = obj_Opciones.Result.filter(function (opcion) {
            return opcion.IdPreguntaOpcion == IdOpcion;
        });;
    }
    else {
        OpcionSeleccionada = pPreguntaOpcion;
        IdOpcion = OpcionSeleccionada[0].IdPreguntaOpcion
        OpcionSeleccionada[0].Descripcion = "";
    }

    if (OpcionSeleccionada.length == 0) {
        return;
    }

    if (OpcionSeleccionada[0].Descripcion == "Otro" || OpcionSeleccionada[0].Descripcion == "Otra") {
        CambiarTipoPregunta(OpcionSeleccionada);
    }
    else {
        if (int_gContadorClicks < lst_gPreguntas.length) {
            int_gContadorClicks++;
        //}
        var obj_gOpcionesSelecc = null;
        obj_gOpcionesSelecc = new Object();
        obj_gOpcionesSelecc.IdPregunta = $("#IdPregunta").val();
        obj_gOpcionesSelecc.IdPreguntaOpcion = IdOpcion;
        obj_gOpcionesSelecc.Observacion = $("#txtAreaRespuesta").val();
        lst_gOpcionesSelecc.push(obj_gOpcionesSelecc);
        $("#txtPregunta").text("");
        }
        if (int_gContadorClicks >= lst_gPreguntas.length) {
            $("#btnEnviar").show();
            $("#btnContinuar").hide();
            obj_OpcionSelec = null;
            FlagContinuar = false;
            $("#txtPregunta").text("");
            $("#txtPreguntaFinal").text("Completa tu información para poder enviarte la propuesta.");
            $("#btnRegresarFin").show();
            $("#IdPanelTitle").hide();
            //$("#IdDetallePregunta").text("");
            //$("#IdDetallePreguntaRes").text("");
            $(".opciones").empty();
            $("#IdFormulario").show();
        }
        else {
            $("#btnRegresarFin").hide();
            $("#btnContinuar").hide();
            $("#btnRegresar").show();
            obj_OpcionSelec = null;
            FlagContinuar = false;
            $("#txtPregunta").text(lst_gPreguntas[int_gContadorClicks].Descripcion);
            //$("#IdDetallePregunta").text(lst_gPreguntas[int_gContadorClicks].DetallePregunta);
            // $("#IdDetallePreguntaRes").text(lst_gPreguntas[0].DetallePregunta);
            $('#IdConsulta').tooltip('dispose');
            if (lst_gPreguntas[int_gContadorClicks].DetallePregunta != null) {
                $('#IdConsulta').tooltip({ 'title': lst_gPreguntas[int_gContadorClicks].DetallePregunta });
            } else {
                $('#IdConsulta').tooltip({ 'title': '' });
            }
            $("#IdPregunta").val("");
            $("#IdPregunta").val(lst_gPreguntas[int_gContadorClicks].IdPregunta);
            $(".opciones").empty();
            //$("#IDLi0").removeClass("activo");//.addClass("yourClass");
            //var id = "#IDLi" + int_gContadorClicks;
            //$(id).addClass("activo");
            CargarOpciones(lst_gPreguntas[int_gContadorClicks].IdPregunta);
        }
    }
}

var obj_OpcionSelec = null
function CambiarTipoPregunta(pOpcionSeleccionada) {
    $("#btnContinuar").show();
    $("#btnRegresarFin").hide();
    $("#btnRegresar").show();
    $('#IdConsulta').tooltip('dispose');
    $(".opciones").empty();

    var divImagen = null;
    var divOpciones = null;
    var input = null;
    var img = null;
    var ulListado = null;
    var lblOtro = null;
    var txtAreaOtro = null;
    var strTipoPregunta = null;
    strTipoPregunta = obj_Opciones.Result[0].TipoPregunta;
    //divImagen = $("<div class='col-xl-4 col-md-12 imgPregunta'></div>");
    //img = $("<img src='Imagen/" + obj_Opciones.Result[0].Imagen + "'" + "' class='img-fluid' /div>");
    //divImagen.append(img);
    //$(".opciones").append(divImagen);
    divOpciones = $("<div class='col-xl-8 col-md-12' style='color:white;' ></div>");
    //dividir el 100/obj_Opciones.Result.length
    var withbtn = 80 / obj_Opciones.Result.length;
    input = $("<input type='hidden' value='" + pOpcionSeleccionada[0].IdPreguntaOpcion + "' class='IdOpcion' /div>");
    ulListado = $("<div class='imgOpcion' style='margin-top: 10px;height:" + parseInt(withbtn) + "%;'></div>");
    if (pOpcionSeleccionada[0].Descripcion != null) {
        lblOtro = $("<label style='font-family: Lato, sans-serif; font-size: 24px; color: black;'>" + pOpcionSeleccionada[0].Descripcion + "</label> <br />");
    }
    txtAreaOtro = $("<textarea id='txtAreaRespuesta' class='txtAreaRespuesta' style='white-space: normal;height: 100%;width: 100%;resize:none' maxlength='500' rows='5' data-min-rows='1'></textarea>");
    ulListado.append(input);
    ulListado.append(lblOtro);
    ulListado.append(txtAreaOtro);
    divOpciones.append(ulListado);
    $(".opciones").append(divOpciones);

    obj_OpcionSelec = null;
    obj_OpcionSelec = pOpcionSeleccionada;
}

var obj_Opciones = null;
function CargarOpciones(int_pIdPregunta) {
    var idPregunta = parseInt(int_pIdPregunta)
    if (idPregunta != null) {
        obj_Opciones = null;
        obj_Opciones = ObtenerData("Default.aspx/ListarOpcionxPregunta", { "int_IdPregunta": idPregunta });

        if (obj_Opciones != null) {
            var divImagen = null;
            var divOpciones = null;
            var input = null;
            var img = null;
            var ulListado = null;
            var liListado = null;
            var txtAreaOtro = null;
            var strTipoPregunta = null;
            strTipoPregunta = obj_Opciones.Result[0].TipoPregunta;
            //divImagen = $("<div class='col-xl-4 col-md-12 imgPregunta'></div>");
            //img = $("<img src='Imagen/" + obj_Opciones.Result[0].Imagen + "'" + "' class='img-fluid' /div>");
            //divImagen.append(img);
            //$(".opciones").append(divImagen);
            divOpciones = $("<div class='col-xl-8 col-md-12' style='color:white;' ></div>");
            for (var i = 0; i < obj_Opciones.Result.length; i++) {
                //dividir el 100/obj_Opciones.Result.length
                var withbtn = 80 / obj_Opciones.Result.length;

                if (strTipoPregunta == "TEXTAREA") {
                    CambiarTipoPregunta(obj_Opciones.Result);
                }
                else {
                    input = $("<input type='hidden' value='" + obj_Opciones.Result[i].IdPreguntaOpcion + "' class='IdOpcion' /div>");
                    ulListado = $("<div class='imgOpcion' style='margin-top: 10px;height:" + parseInt(withbtn) + "%;'></div>");

                    liListado = $("<button type='button' class='btn BotonDescripcion btn-secondary btn-block' style='white-space: normal;height: 100%;text-align:left'>" + obj_Opciones.Result[i].Descripcion + "</button >");
                    ulListado.append(liListado);
                    ulListado.append(input);
                    divOpciones.append(ulListado);

                    $(".opciones").append(divOpciones);

                    $('.opciones').each(function () {
                        var link = $('.imgOpcion');
                        link.click(CambiarPregunta);
                    });
                    //$(".opciones").html(div.html());
                }
            }
        }
    }

}

function ListarPreguntas() {
    AccionDefault(true, "Default.aspx/ListarPregunta", {}, CargarListado, null, null, null, 1);

}

function CargarListado(obj_pLista) {
    if (obj_pLista.Result.length != 0) {
        int_gCantidadPreguntas = obj_pLista.Result.length;
        lst_gPreguntas = obj_pLista.Result;
        var li = null;
        var addli = null;
        /*for (var i = 1; i < int_gCantidadPreguntas; i++) {
            addli = $("<li class='numeracion' id=IDLi" + i + "></li>");
            $("#progreso").append(addli);
        }*/

        $("#txtPregunta").text("");
        //$("#btnRegresar").show();

        //$("#txtPregunta").append("<a class='btn' data-toggle='tooltip' data-placement='bottom' id='IdConsulta'><img src='Imagen/question2.png' /></a>");
        $("#txtPregunta").text(lst_gPreguntas[0].Descripcion);
        // 
        $('#IdConsulta').tooltip({ 'title': lst_gPreguntas[0].DetallePregunta });
        //$("#IdDetallePregunta").text(lst_gPreguntas[0].DetallePregunta);
        //$("#IdDetallePreguntaRes").text(lst_gPreguntas[0].DetallePregunta);
        $("#IdPregunta").val(lst_gPreguntas[0].IdPregunta);
        CargarOpciones(lst_gPreguntas[0].IdPregunta)
    }


}
﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="Scripts/index.js"></script>
    <script src="Scripts/Default.js"></script>
    <script src="Scripts/Base/jquery/jquery.min.js"></script>
    <script src="Scripts/Base/jquery.validate/jquery.validate.js"></script>
    <script src="Style/bootstrap-4.0.0/dist/js/popper.min.js"></script>
    <script src="Style/bootstrap-4.0.0/dist/js/bootstrap.js"></script>
    
   <div id="contenedor" class="container-fluid">
        <article>
            <div>
                <div>
                    <div>
                        <section>
                            <div class="row">
                                <div class="col">
                                    <section>
                                        <div class="row">
                                            <div class="col">
                                                <div id="Titulo">
                                                    <p><br /></p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row" id="IdPanelTitle">
                                                <div class="col-12 col-md-10 Subtitle">
                                                    <div class="row">
                                                        <div class="col-9">
                                                            <label id="txtPregunta" class="PreguntaDB"></label>
                                                            <%--<a class="btn" data-toggle="tooltip" data-placement="bottom" id="IdConsulta"><img src="Imagen/question2.png" /></a>--%>
                                                            <input type="hidden" id="IdPregunta"/>
                                                        </div>
                                                       <%-- <div class="col-3">
                                                            <a class="btn" data-toggle="tooltip" data-placement="bottom" id="IdConsulta"><img class="imgCuestion" src="Imagen/question2.png" /></a>
                                                        </div>--%>
                                                    </div>
                                                    <div class="row opciones" >

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-8 col-lg-4 col-xl-8" style="text-align:center">
                                                               <button type="button" id="btnRegresar" class="btn btn-secondary btnRegresar" style="float: none; margin:10px">
                                                                   <%--<span><img src="Imagen/1x/regresar.png"></span>--%>
                                                                   Regresar</button>
                                                            <button type="button" id="btnContinuar" class="btn btn-secondary btnContinuar" style="float: none; margin: 10px">
                                                                   Continuar
                                                                <%--<span><img src="Imagen/1x/continuar.png"></span>--%>
                                                            </button>
                                                        </div>
                                                        <%--<div class="elementoDiv col">
                                                            <ul id="progreso">
                                                                <li class="activo numeracion" id="IDLi0"></li>
                                                            </ul>
                                                        </div>--%>
                                                        <%--<div class="col-5">
                                                            <button type="button" id="btnContinuar" class="btn btn-secondary btnContinuar" style="float: left; margin: 10px">
                                                                   Continuar
                                                                <%--<span><img src="Imagen/1x/continuar.png"></span>--
                                                            </button>
                                                        </div>--%>
                                                    </div>

                                                    <%--<div class="row">
                                                        <div class="col-5">
                                                            <button type="button" id="btnContinuar" class="btn btn-secondary btnContinuar" style="float: right; margin: 10px">
                                                                   Continuar
                                                                <span><img src="Imagen/1x/continuar.png"></span>
                                                            </button>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                        </div>
                                        <div class="row" id="detaRespon">
                                           <div class="col-12 Subtitle">
                                               <br>
                                              <label id="IdDetallePreguntaRes"></label>
                                           </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-10 FormularioFinal" id="IdFormulario">
                                                <label id="txtPreguntaFinal" class="PreguntaDB"></label>
                                                <div>
                                                    <div class="form-row" style="text-align:left;">
                                                        <div class="col-md-12">
                                                            <label for="inputEmail4" style="font-family: 'Lato', sans-serif;; font-size: 24px;">Nombre</label>
                                                            <input type="text" class="form-control soloLetrasYEspacio" name="txtNombreC" id="txtNombreC">
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="inputEmail4" style="font-family: 'Lato', sans-serif;; font-size: 24px;">Correo</label>
                                                            <input type="text" class="form-control" name="txtMail" id="txtMail">
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="inputTelefono" style="font-family: 'Lato', sans-serif;; font-size: 24px;">Celular</label>
                                                            <input type="text" class="form-control soloNumeros" maxlength="9" name="txtCelular" id="txtCelular" onkeypress="return valideKey(event)">
                                                        </div>
                                                       <div class="form-group" style="margin-top: 10px;">
                                                           <input type="checkbox" class="form-check-input" id="chkPolitica2" name="chkPolitica2">
                                                          <div class="form-check-label" style="font-family: 'Lato', sans-serif;; font-size: 24px;">
                                                               &nbsp; Acepto la <a href="PoliticaManejoDatos.aspx" target="_blank" style="text-decoration:underline; font-weight:bold;cursor: -webkit-grab; cursor: grab;">Política de Tratamiento de Datos</a>
                                                           </div>
                                                          <input type="hidden" id="chkPolitica" name="chkPolitica">
                                                        </div>
                                                    </div>    
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col" id="IdBotones">
                                                <div class="row">
                                                    <div class="col" id="btnRegresarFin">
                                                        <button type="button" class="btn btn-secondary btnRegresar" style="float: right;">
                                                                   <%--<span><img src="Imagen/1x/regresar.png"></span>--%>
                                                            Regresar</button>
                                                    </div>
                                                    <div class="col">
                                                         <button type="submit" id="btnEnviar" class="btn btn-success btnEnviarfin" style="background-color: white;border-color: white;">
                                                             <%--<span><img src="Imagen/1x/enviar.png"></span>--%>
                                                             Enviar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row filas">
                                            <div class="col-8 Subtitle" id="IdCarMensaje" style="text-align:center;">
                                                <label class="card-text" id="IdMensajeResultado">¡Gracias! Tu información ha sido enviada. Te estaremos enviando los mentores que hacen match a tu correo.</label>
                                                <br />
                                                <a id="btnVolverCotizar" class="btn btn-secondary btnRegresarTer" style="background-color: white;border-color: white;">
                                                    <%--<span><img src="Imagen/1x/regresar.png"></span>--%>
                                                    Volver a buscar</a>
                                            </div>  
                                       </div>
                                    </section>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="modal fade full-color" id="md-template" tabindex="-1" data-keyboard="false" data-backdrop="static" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3></h3>
                                    <button type="button" class="close btn-cerrar" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <div class="i-circle success" style="display: none;"><i class="fa fa-check"></i></div>
                                        <div class="i-circle info" style="display: none;"><i class="fa fa-info"></i></div>
                                        <div class="i-circle warning" style="display: none;"><i class="fa fa-warning"></i></div>
                                        <div class="i-circle danger" style="display: none;"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-flat btn-mono3 btn-aceptar" data-dismiss="modal">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
<script>
    /*function soloNum(e) {
        key = e.keyCode || e.which;
        teclado = String.fromCharCode(key);
        numero = "0123456789";
        especiales = "8-37-38-46";
        teclado_especial = false;

        for (var i in especiales) {
            if (key == especiales[i]) {
                teclado_especial = true;
            }
        }

        if (numero.indexOf(teclado) == -1 && !teclado_especial) {
            return false;
        }
    }*/

    function valideKey(evt) {

        // code is the decimal ASCII representation of the pressed key.
        var code = (evt.which) ? evt.which : evt.keyCode;

        if (code == 8) { // backspace.
            return true;
        } else if (code >= 48 && code <= 57) { // is a number.
            return true;
        } else { // other keys.
            return false;
        }
    }
</script>
</asp:Content>


